import { ListUsersComponent } from './list-users/list-users.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ListServicesComponent } from './list-services/list-services.component';
import { LogsComponent } from '../shared/logs/logs.component';
import { ChartsComponent } from '../shared/charts/charts.component';
const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      { path: 'logs/:service-name', component: LogsComponent },
      { path: 'liste-users', component: ListUsersComponent },
      { path: 'liste-services', component: ListServicesComponent },
      { path: 'charts/:service-name', component: ChartsComponent },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
