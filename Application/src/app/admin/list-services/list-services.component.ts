import { AfterViewInit, Component, OnInit, TemplateRef, ViewChild, inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Service } from 'src/app/models/service.model';
import { AdminService } from 'src/app/services/admin/admin.service';
import { LogService } from 'src/app/services/log/log.service';
import { ServiceService } from 'src/app/services/service/service.service';
@Component({
  selector: 'app-list-services',
  templateUrl: './list-services.component.html',
  styleUrls: ['./list-services.component.css']
})
export class ListServicesComponent {
  x: any;
  service: any;
  selectedService!: Service;
  logs: any[] = []; // Initialize an array to store logs
  currentPage = 1; // Current page number
  pageSize = 10; // Number of items per page
  totalPages = 0;

  services: Service[] = [];
  popform !: FormGroup;
  popform1!: FormGroup;
  private modalService = inject(NgbModal);
  @ViewChild('pop') popRef!: TemplateRef<any>;
  @ViewChild('pop1') popRef1!: TemplateRef<any>;

  openAjout() {
    this.initForm1();
    this.modalService.open(this.popRef1, { backdropClass: 'pop-up-backdrop' });
  }
  listeServices!: any[];
  constructor(private ss: ServiceService, private ls: LogService, private router: ActivatedRoute, private rt: Router, private formbuild: FormBuilder) { }


  ngOnInit(): void {
    this.loadServices();
    this.initForm();
    this.initForm1();


  }
  loadServices() {
    this.ss.getServices().subscribe(data => {
      this.listeServices = data;
    });
  }
  initForm() { // edit
    this.popform = this.formbuild.group({
      id_service: ['', Validators.required],
      name: ['', Validators.required],
      oldName: ['', Validators.required],
      input: ['', Validators.required],
      filter: ['', Validators.required]
    });
  }
  initForm1() { // add
    this.popform1 = this.formbuild.group({
      // id_service: ['', Validators.required],
      name: ['', Validators.required],
      input: ['', Validators.required],
      filter: ['', Validators.required]
    });
  }

  onModifie(i: any) {
    this.ss.getServiceById(i.id_service).subscribe(
      (res: any) => {
        this.service = res[0];
        this.popform.patchValue({
          oldName: i.name,
          id_service: i.id_service,
          name: i.name,
          input: i.input,
          filter: i.filter,
          output: 'elasticsearch{hosts=>"elasticsearch:9200"index=>"' + i.name + '"}'
        });
      }
    );
    this.modalService.open(this.popRef, { backdropClass: 'pop-up-backdrop' });

  }
  onSubmit() { // submit the edit
    //  const formData=this.popform.value;
    let formData: any = {
      oldName: this.popform.value.oldName,
      id_service: this.popform.value.id_service,
      name: this.popform.value.name,
      input: this.popform.value.input,
      filter: this.popform.value.filter,
      output: 'elasticsearch{hosts=>"elasticsearch:9200"index=>"' + this.popform.value.name + '"}'
    }

    this.ss.editService(formData).subscribe(() => {
      // this.popform.reset();
      this.modalService.dismissAll();
      console.log("Update Successfully");
      console.log(formData);
      this.ngOnInit();
    }, error => {
      console.error('Error while updating service', error);
      alert("Error while updating service");
    })
    //  console.log(formData);

  }
  onAjout() { // submit the add

    let formData = this.popform1.value;
    formData.output = 'elasticsearch{hosts=>"elasticsearch:9200"index=>"' + formData.name + '"}'
    if (this.popform1.valid) {
      this.ss.addService(formData).subscribe(() => {
        this.modalService.dismissAll();
        console.log("Add Successfully");
        this.ngOnInit();
      }, error => {
        console.error('Error while adding service :', error);
        alert("Error while adding service");
      })
    }
    this.modalService.open(this.popRef1, { backdropClass: 'pop-up-backdrop' });
    console.log(formData);
  }

  onDeleteService(id: number, name: string): void {

    this.ss.deleteService(id, name).subscribe(() => {
      // Remove the deleted service from the local array to reflect changes immediately
      this.listeServices = this.listeServices.filter(service => service.id_service !== id);
      console.log('Delete service successfully');

    }, error => {
      console.error('Error deleting service:', error);
      // Optionally, handle error (e.g., display error message)
    });
  }


  /* viewLogs(serviceName: string): void {
     this.rt.navigate(['/admin/logs', serviceName]);
   }*/


  viewLogs(serviceName: string): void {
    this.rt.navigate(['/admin/logs/', serviceName]);
  }

  viewCharts(serviceName: string): void {
    this.rt.navigate(['/admin/charts/', serviceName]);
  }
  //this.rt.navigate(['/admin/charts/', serviceName]);
}

