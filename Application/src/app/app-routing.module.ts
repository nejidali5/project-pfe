import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './shared/login/login.component';
import { SignUpComponent } from './shared/sign-up/sign-up.component';
import { adminGuard } from './guards/admin.guard';
import { LayoutComponent } from './shared/layout/layout.component';
import { ChartsComponent } from './shared/charts/charts.component';
import { userGuard } from './guards/user.guard';


const routes: Routes = [




  { path: 'home', component: LayoutComponent },
  { path: 'admin', loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule), canActivate: [adminGuard] },
  { path: 'user', loadChildren: () => import('./user/user.module').then(m => m.UserModule), canActivate: [userGuard] },


  { path: 'register', component: SignUpComponent },
  { path: 'login', component: LoginComponent },
  { path: 'charts', component: ChartsComponent },
  { path: '**', redirectTo: 'home', pathMatch: 'full' },
  // { path: '**', redirectTo: '404' },


];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
