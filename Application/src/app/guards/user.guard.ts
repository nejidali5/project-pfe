import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { map, catchError, of } from 'rxjs';
import { LoginService } from '../services/login/login.service';

export const userGuard: CanActivateFn = (route, state) => {
  const router = inject(Router)
  const loginService = inject(LoginService);

  return loginService.role().pipe(map((val) => {
    if (val == "user") return true;
    else {
      router.navigate(['/home'])
      alert("You can't access this page")
      return false
    }
  }),
    catchError((error) => {
      console.log(error);
      router.navigate(['/home'])
      return of(false);
    }));
};
