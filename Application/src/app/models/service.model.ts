// service.model.ts
export interface Service {
  id_service: number;
  name: string;
  input: string;
  filter: string;
  output: string;
}
