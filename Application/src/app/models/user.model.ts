// user.model.ts
export interface User {
  id_user: number;
  nom: string;
  prenom: string;
  email: string;
  password: string;
  departement: string;
  contact: string;

}
