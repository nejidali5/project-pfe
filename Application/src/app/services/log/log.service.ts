import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  constructor(private http: HttpClient) { }
  private url: string = environment.apiUrl;
  /*
  getLogs(page: number, size: number, filter?: any): Observable<any> {
    let params = new HttpParams()
      .set('page', page.toString())
      .set('size', size.toString());

    if (filter) {
      params = params.set('filter', JSON.stringify(filter));
    }

    return this.http.get(this.url, { params });*/

  getLogs(serviceName: string): Observable<any> {
    return this.http.get(`${this.url}/api/logs/${serviceName}`);
  }

}
