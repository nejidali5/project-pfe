import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Service } from '../../models/service.model';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http: HttpClient) { }
  private url: string = environment.apiUrl;
  options = {
    headers: new HttpHeaders(
      { 'content-type': "application/json" }
    )
  }

  // Services
  getServices(): Observable<any[]> {
    return this.http.get<any[]>(this.url + "/api/services", this.options).pipe(catchError(err => {
      console.error('Error Connection:', err);
      return of([]);
    }))
  }

  //Service
  getServiceById(id: number): Observable<Service> {
    return this.http.get<Service>(`${this.url}/api/services/${id}`);
  }

  editService(i: any): Observable<any> {
    return this.http.post<any>(this.url + "/api/services/editService", i, this.options);
  }
  addService(data: any): Observable<any> {
    return this.http.post<any>(this.url + "/api/services/addService", data, this.options);
  }
  /*
    DeleteService(id: string) {
      return this.http.post<any>(`${this.url}/deleteService`, { id_service: +id }, this.options).pipe(
        catchError(err => {
          console.log(err)
          alert("Error while deleting service");
          return of([]);
        })
      );
    }*/
  /*
 deleteService(idService: any) {
   return this.http.delete(this.url + "/deleteService", idService);
 }*/

  //Delete v2
  /*
    deleteService(id: string): Observable<any> {
      // Assuming you have a JSON object with the ID
      const payload = { id: id };
      return this.http.post(`${this.url}/deleteService`, payload);
    }*/
  deleteService(id: number, name: string): Observable<any> {
    return this.http.delete<any>(`${this.url}/api/services/${id}/${name}`);
  }


  //updateLogstashConfig
  updateLogstashConfig(configData: string) {
    return this.http.post<any>('updatelogstashconfig', { config: configData });
  }
}
/*configure
writeConfigToFile(id: number) {
  return this.http.post(`${this.url}/writeConfigToFile`, { id_service: +id });
}*/

