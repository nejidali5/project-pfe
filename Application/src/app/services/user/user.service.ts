import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
  private url: string = environment.apiUrl;
  options = {
    headers: new HttpHeaders(
      { 'content-type': "application/json" }
    )
  }




  // Users
  getUsers(): Observable<any[]> {
    return this.http.get<any[]>(this.url + "/api/users", this.options).pipe(catchError(err => {
      console.error('Error Connexion:', err);
      return of([]);
    }))
  }
  getUserById(id: number): Observable<User> {
    return this.http.get<User>(`${this.url}/api/users/${id}`);
  }
  addUser(data: any): Observable<any> {
    return this.http.post<any>(this.url + "/api/users/addUser", data, this.options);
  }
  editUser(i: any): Observable<any> {
    return this.http.post<any>(this.url + "/api/users/editUser", i, this.options);
  }

  deleteUser(id: number): Observable<any> {
    return this.http.delete<any>(`${this.url}/api/users/${id}`);
  }


}
