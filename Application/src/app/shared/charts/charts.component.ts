import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import Chart, { ChartData, ChartOptions } from 'chart.js/auto';
import { ChartsService } from 'src/app/services/charts/charts.service';
import { LogService } from 'src/app/services/log/log.service';
import * as moment from 'moment';
import 'chartjs-adapter-moment';
import 'chartjs-plugin-datalabels';
import { ChoroplethController, GeoFeature } from 'chartjs-chart-geo';

Chart.register(ChoroplethController, GeoFeature);


@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit {
  @ViewChild('logChart') private chartRef: ElementRef | undefined;
  @ViewChild('lineChart') private lineChartRef!: ElementRef;
  @ViewChild('pieChart') private pieChartRef!: ElementRef;
  @ViewChild('appChart') private appChartRef!: ElementRef;
  @ViewChild('policyTypeChart') private policyTypeChartRef!: ElementRef;
  @ViewChild('sourceDestinationChart') private sourceDestinationChartRef!: ElementRef;
  @ViewChild('serviceCountChart') private serviceCountChartRef!: ElementRef;
  @ViewChild('serviceCountByTimeChart') private serviceCountByTimeChartRef!: ElementRef;
  @ViewChild('threatLevelChart') private threatLevelChartRef!: ElementRef;
  @ViewChild('sourceDestinationIPChart') private sourceDestinationIPChartRef!: ElementRef;
  @ViewChild('queryCountChart') queryCountChartRef!: ElementRef;
  @ViewChild('hostActivityChart') hostActivityChartRef!: ElementRef;
  @ViewChild('sourceIPCountChart') sourceIPCountChartRef!: ElementRef;
  @ViewChild('eventDatasetChart') eventDatasetChartRef!: ElementRef;
  @ViewChild('agentVersionChart') agentVersionChartRef!: ElementRef;
  @ViewChild('logFilePathChart') logFilePathChartRef!: ElementRef
  @ViewChild('hostOSVersionChart') hostOSVersionChartRef!: ElementRef
  @ViewChild('timeSeriesChart') private timeSeriesChartRef!: ElementRef;
  @ViewChild('queryUserDistributionChart') private queryUserDistributionChartRef!: ElementRef;
  @ViewChild('heatmapChart') private heatmapChartRef!: ElementRef;






  private lineChart: Chart | undefined;
  private pieChart: Chart | undefined;
  private chart: Chart | undefined;
  private appChart: Chart | undefined;
  private policyTypeChart: Chart | undefined;
  private sourceDestinationChart: Chart | undefined;
  private serviceCountChart: Chart | undefined;
  private serviceCountByTimeChart: Chart | undefined;
  private threatLevelChart: Chart | undefined;
  private sourceDestinationIPChart: Chart | undefined;
  private queryCountChart: Chart | undefined;
  private hostActivityChart: Chart | undefined;
  private sourceIPCountChart: Chart | undefined;
  eventDatasetChart: Chart | undefined;
  agentVersionChart: Chart | undefined;
  logFilePathChart: Chart | undefined;
  hostOSVersionChart: Chart | undefined;
  private timeSeriesChart: Chart | undefined;
  private queryUserDistributionChart: Chart | undefined;
  private heatmapChart: Chart | undefined;

  serviceName!: string;
  logs: any[] = [];

  constructor(private route: ActivatedRoute, private ls: LogService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.serviceName = params['service-name'];
      this.viewCharts(this.serviceName);
    });
  }

  viewCharts(serviceName: string): void {
    this.ls.getLogs(serviceName).subscribe(
      data => {
        if (this.processLogsData(data)) {
          const logType = this.getLogType(serviceName);
          this.renderCharts(logType);
          //this.renderChart();
          //  this.renderLineChart();
          //this.renderPieChart();
          //this.renderSourceDestinationChart();
          //this.renderServiceCountChart();
          //this.renderServiceCountByTimeChart();
          //this.renderThreatLevelChart();
          //this.renderSourceDestinationIPChart();
          // this.renderAppChart();
          // this.renderPolicyTypeChart();
        }
      },
      error => {
        console.error('Error fetching logs', error);
      }
    );
  }
  getLogType(serviceName: string): string {
    // Determine log type based on service name
    if (serviceName.toLowerCase().includes('fortigate')) {
      return 'fortigate';
    } else if (serviceName.toLowerCase().includes('mysql')) {
      return 'mysql';
    }
    return 'unknown';
  }

  processLogsData(data: any): boolean {
    if (data && data.response && data.response.hits && data.response.hits.hits) {
      this.logs = data.response.hits.hits.map((log: any) => ({
        ...log,
        showDetails: false
      }));

      // Log the logs array to inspect its structure
      console.log('Logs:', this.logs);

      // Return true if logs are available, false otherwise
      return this.logs && this.logs.length > 0;
    } else {
      console.error('Invalid data format received:', data);
      return false;
    }
  }
  renderCharts(logType: string): void {
    switch (logType) {
      case 'fortigate':
        this.renderFortigateCharts();
        break;
      case 'mysql':
        this.renderMySQLCharts();
        break;
      default:
        console.error('Unknown log type:', logType);
    }
  }

  renderFortigateCharts(): void {
    this.renderChart();
    this.renderPieChart();
    this.renderSourceDestinationChart();
    this.renderServiceCountChart();
    this.renderServiceCountByTimeChart();
    this.renderThreatLevelChart();
    this.renderSourceDestinationIPChart();
  }

  renderMySQLCharts(): void {
    this.renderQueryCountChart();
    this.renderHostActivityChart();
    this.renderEventDatasetChart();
    this.renderAgentVersionChart();
    this.renderLogFilePathChart();
    this.renderHostOSVersionChart();
    this.renderTimeSeriesChart();
    // this.renderQueryUserDistributionChart();
    //this.renderHeatmapChart()

  }

  /*renderQueryUserDistributionChart(): void {
    const userQueryCounts = this.logs.reduce((acc, log) => {
      const user = log.fields?.['user.name']?.[0];
      if (user) {
        if (!acc[user]) {
          acc[user] = 0;
        }
        acc[user]++;
      }
      return acc;
    }, {});

    const labels = Object.keys(userQueryCounts);
    const data = Object.values(userQueryCounts);

    const chartData: ChartData = {
      labels: labels,
      datasets: [{
        label: 'User Query Distribution',
        data: data,
        backgroundColor: labels.map(() => this.getRandomColor()),
        hoverOffset: 4
      }]
    };

    const chartOptions: ChartOptions = {
      responsive: true,
      plugins: {
        legend: {
          position: 'top',
        },
        title: {
          display: true,
          text: 'Query Distribution by User'
        }
      }
    };

    if (this.queryUserDistributionChart) {
      this.queryUserDistributionChart.destroy();
    }

    this.queryUserDistributionChart = new Chart(this.queryUserDistributionChartRef.nativeElement, {
      type: 'pie',
      data: chartData,
      options: chartOptions
    });
  }*/

  /* renderHeatmapChart(): void {
     const activityData = this.logs.reduce((acc, log) => {
       const timestamp = log.fields?.['@timestamp']?.[0];
       const user = log.fields?.['user.name']?.[0];
       if (timestamp && user) {
         const time = moment(timestamp).startOf('minute').toISOString(); // Adjust the time unit as needed
         if (!acc[time]) {
           acc[time] = {};
         }
         if (!acc[time][user]) {
           acc[time][user] = 0;
         }
         acc[time][user]++;
       }
       return acc;
     }, {});
 
     const labels = Object.keys(activityData).sort();
     const users = Array.from(new Set(this.logs.map(log => log.fields?.['user.name']?.[0])));
 
     const data = labels.map(time => users.map(user => activityData[time][user] || 0));
 
     const chartData: ChartData = {
       labels: users,
       datasets: [{
         label: 'Activity Levels',
         data: data.flat(),
         borderColor: 'rgba(0, 0, 0, 0.1)',
         backgroundColor: 'rgba(0, 0, 255, 0.5)'
       }]
     };
 
     const chartOptions: ChartOptions = {
       responsive: true,
       scales: {
         x: {
           type: 'category',
           labels: labels
         },
         y: {
           type: 'category',
           labels: users
         }
       }
     };
 
     if (this.heatmapChart) {
       this.heatmapChart.destroy();
     }
 
     this.heatmapChart = new Chart(this.heatmapChartRef.nativeElement, {
       type: 'heatmap',
       data: chartData,
       options: chartOptions
     });
   }*/

  renderTimeSeriesChart(): void {
    const timeSeriesData = this.logs.reduce((acc, log) => {
      const timestamp = log.fields?.['@timestamp']?.[0];
      if (timestamp) {
        const time = moment(timestamp).startOf('minute').toISOString(); // Adjust the time unit as needed
        if (!acc[time]) {
          acc[time] = 0;
        }
        acc[time]++;
      }
      return acc;
    }, {});

    const labels = Object.keys(timeSeriesData).sort();
    const data = labels.map(label => timeSeriesData[label]);

    const chartData: ChartData = {
      labels: labels,
      datasets: [{
        label: 'Number of Queries',
        data: data,
        fill: false,
        borderColor: 'rgba(75, 192, 192, 1)',
        tension: 0.1
      }]
    };

    const chartOptions: ChartOptions = {
      responsive: true,
      scales: {
        x: {
          type: 'time',
          time: {
            unit: 'minute'
          }
        },
        y: {
          beginAtZero: true
        }
      }
    };

    if (this.timeSeriesChart) {
      this.timeSeriesChart.destroy();
    }

    this.timeSeriesChart = new Chart(this.timeSeriesChartRef.nativeElement, {
      type: 'line',
      data: chartData,
      options: chartOptions
    });
  }




  renderQueryCountChart(): void {
    const queryCountData = this.logs.reduce((acc, log) => {
      const originalEvent = log.fields?.['event.original']?.[0];
      if (originalEvent) {
        // Parse the key-value pairs from the log string
        const keyValuePairs = originalEvent.split(' ');
        keyValuePairs.forEach((pair: { split: (arg0: string) => [any, any]; }) => {
          const [key, value] = pair.split('=');
          if (key === 'type') {  // Assuming 'type' represents the query type in your logs
            const queryType = value?.replace(/"/g, ''); // Remove quotes if any
            if (queryType) {
              acc[queryType] = (acc[queryType] || 0) + 1;
            }
          }
        });
      }
      return acc;
    }, {});

    console.log('Query Count Data:', queryCountData); // Debugging

    const chartData: ChartData = {
      labels: Object.keys(queryCountData),
      datasets: [{
        label: 'Query Count',
        data: Object.values(queryCountData),
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)'
        ],
        borderColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
        ],
        borderWidth: 1
      }]
    };

    const chartOptions: ChartOptions = {
      responsive: true,
      plugins: {
        legend: {
          position: 'top',
        },
        title: {
          display: true,
          text: 'Query Count Distribution'
        },
        tooltip: {
          callbacks: {
            label: function (tooltipItem: any) {
              return `${tooltipItem.label}: ${tooltipItem.raw}`;
            }
          }
        }
      },
      scales: {
        y: {
          beginAtZero: true,
          title: {
            display: true,
            text: 'Count'
          }
        },
        x: {
          title: {
            display: true,
            text: 'Query Type'
          }
        }
      }
    };

    if (this.queryCountChart) {
      this.queryCountChart.destroy();
    }

    this.queryCountChart = new Chart(this.queryCountChartRef.nativeElement, {
      type: 'bar',
      data: chartData,
      options: chartOptions,
    });
  }






  renderHostActivityChart(): void {
    const hostActivityData = this.logs.reduce((acc, log) => {
      const host = log.fields?.['host.name']?.[0];
      if (host) {
        acc[host] = (acc[host] || 0) + 1;
      }
      return acc;
    }, {});

    console.log('Host Activity Data:', hostActivityData);

    const chartData: ChartData = {
      labels: Object.keys(hostActivityData),
      datasets: [{
        label: 'Host Activity',
        data: Object.values(hostActivityData),
        backgroundColor: 'rgba(153, 102, 255, 0.2)',
        borderColor: 'rgba(153, 102, 255, 1)',
        borderWidth: 1
      }]
    };

    const chartOptions: ChartOptions = {
      responsive: true,
      plugins: {
        legend: {
          position: 'top',
        },
        title: {
          display: true,
          text: 'Host Activity Distribution'
        }
      }
    };

    if (this.hostActivityChart) {
      this.hostActivityChart.destroy();
    }

    this.hostActivityChart = new Chart(this.hostActivityChartRef.nativeElement, {
      type: 'pie',
      data: chartData,
      options: chartOptions,
    });
  }

  renderEventDatasetChart(): void {
    const eventDatasetData = this.logs.reduce((acc, log) => {
      const eventDataset = log.fields?.['event.dataset']?.[0];
      if (eventDataset) {
        acc[eventDataset] = (acc[eventDataset] || 0) + 1;
      }
      return acc;
    }, {});

    const chartData: ChartData = {
      labels: Object.keys(eventDatasetData),
      datasets: [{
        label: 'Event Dataset Distribution',
        data: Object.values(eventDatasetData),
        backgroundColor: 'rgba(255, 99, 132, 0.2)',
        borderColor: 'rgba(255, 99, 132, 1)',
        borderWidth: 1
      }]
    };

    const chartOptions: ChartOptions = {
      responsive: true,
      plugins: {
        legend: {
          position: 'top',
        },
        title: {
          display: true,
          text: 'Event Dataset Distribution'
        }
      }
    };

    if (this.eventDatasetChart) {
      this.eventDatasetChart.destroy();
    }

    this.eventDatasetChart = new Chart(this.eventDatasetChartRef.nativeElement, {
      type: 'doughnut',
      data: chartData,
      options: chartOptions,
    });
  }
  renderAgentVersionChart(): void {
    const agentVersionData = this.logs.reduce((acc, log) => {
      const agentVersion = log.fields?.['agent.version']?.[0];
      if (agentVersion) {
        acc[agentVersion] = (acc[agentVersion] || 0) + 1;
      }
      return acc;
    }, {});

    const chartData: ChartData = {
      labels: Object.keys(agentVersionData),
      datasets: [{
        label: 'Agent Version Distribution',
        data: Object.values(agentVersionData),
        backgroundColor: 'rgba(54, 162, 235, 0.2)',
        borderColor: 'rgba(54, 162, 235, 1)',
        borderWidth: 1
      }]
    };

    const chartOptions: ChartOptions = {
      responsive: true,
      plugins: {
        legend: {
          position: 'top',
        },
        title: {
          display: true,
          text: 'Agent Version Distribution'
        }
      }
    };

    if (this.agentVersionChart) {
      this.agentVersionChart.destroy();
    }

    this.agentVersionChart = new Chart(this.agentVersionChartRef.nativeElement, {
      type: 'pie',
      data: chartData,
      options: chartOptions,
    });
  }
  renderLogFilePathChart(): void {
    const logFilePathData = this.logs.reduce((acc, log) => {
      const logFilePath = log.fields?.['log.file.path']?.[0];
      if (logFilePath) {
        acc[logFilePath] = (acc[logFilePath] || 0) + 1;
      }
      return acc;
    }, {});

    const chartData: ChartData = {
      labels: Object.keys(logFilePathData),
      datasets: [{
        label: 'Log File Path Distribution',
        data: Object.values(logFilePathData),
        backgroundColor: 'rgba(255, 206, 86, 0.2)',
        borderColor: 'rgba(255, 206, 86, 1)',
        borderWidth: 1
      }]
    };

    const chartOptions: ChartOptions = {
      responsive: true,
      plugins: {
        legend: {
          position: 'top',
        },
        title: {
          display: true,
          text: 'Log File Path Distribution'
        }
      }
    };

    if (this.logFilePathChart) {
      this.logFilePathChart.destroy();
    }

    this.logFilePathChart = new Chart(this.logFilePathChartRef.nativeElement, {
      type: 'bar',
      data: chartData,
      options: chartOptions,
    });
  }
  renderHostOSVersionChart(): void {
    const hostOSVersionData = this.logs.reduce((acc, log) => {
      const hostOSVersion = log.fields?.['host.os.version']?.[0];
      if (hostOSVersion) {
        acc[hostOSVersion] = (acc[hostOSVersion] || 0) + 1;
      }
      return acc;
    }, {});

    const chartData: ChartData = {
      labels: Object.keys(hostOSVersionData),
      datasets: [{
        label: 'Host OS Version Distribution',
        data: Object.values(hostOSVersionData),
        backgroundColor: 'rgba(75, 192, 192, 0.2)',
        borderColor: 'rgba(75, 192, 192, 1)',
        borderWidth: 1
      }]
    };

    const chartOptions: ChartOptions = {
      responsive: true,
      plugins: {
        legend: {
          position: 'top',
        },
        title: {
          display: true,
          text: 'Host OS Version Distribution'
        }
      }
    };

    if (this.hostOSVersionChart) {
      this.hostOSVersionChart.destroy();
    }

    this.hostOSVersionChart = new Chart(this.hostOSVersionChartRef.nativeElement, {
      type: 'doughnut',
      data: chartData,
      options: chartOptions,
    });
  }






  getActionCounts(logs: any[]): { [key: string]: number } {
    const actionCounts: { [key: string]: number } = {};

    logs.forEach((log: any) => {
      const action = log.fields?.action?.[0];
      if (action) {
        if (actionCounts[action]) {
          actionCounts[action]++;
        } else {
          actionCounts[action] = 1;
        }
      } else {
        console.warn('Log entry with missing or undefined action:', log);
      }
    });

    return actionCounts;
  }

  renderChart(): void {
    if (!this.chartRef || !this.chartRef.nativeElement) {
      console.error('Chart reference not found');
      return;
    }

    if (!this.logs || this.logs.length === 0) {
      console.error('No logs data available');
      return;
    }

    const actionCounts = this.getActionCounts(this.logs);
    const actionLabels = Object.keys(actionCounts);
    const actionData = Object.values(actionCounts);

    const chartData: ChartData = {
      labels: actionLabels,
      datasets: [
        {
          label: 'Action Counts',
          data: actionData,
          backgroundColor: 'rgba(75, 192, 192, 0.2)',
          borderColor: 'rgba(75, 192, 192, 1)',
          borderWidth: 1,
        },
      ],
    };

    const chartOptions: ChartOptions = {
      responsive: true,
      scales: {
        y: {
          beginAtZero: true,
        },
      },
    };

    if (this.chart) {
      this.chart.destroy();
    }

    this.chart = new Chart(this.chartRef.nativeElement, {
      type: 'bar',
      data: chartData,
      options: chartOptions,
    });
  }

  renderPieChart(): void {
    if (!this.pieChartRef! || !this.pieChartRef!.nativeElement) {
      console.error('Chart reference not found');
      return;
    }

    if (!this.logs || this.logs.length === 0) {
      console.error('No logs data available');
      return;
    }

    const actionCounts = this.getActionCounts(this.logs);
    const actionLabels = Object.keys(actionCounts);
    const actionData = Object.values(actionCounts);

    const chartData: ChartData = {
      labels: actionLabels,
      datasets: [
        {
          label: 'Action Distribution',
          data: actionData,
          backgroundColor: [
            'rgba(255, 99, 132, 0.5)',
            'rgba(54, 162, 235, 0.5)',
            'rgba(255, 206, 86, 0.5)',
            'rgba(75, 192, 192, 0.5)',
            'rgba(153, 102, 255, 0.5)',
          ],
          hoverOffset: 4,
        },
      ],
    };

    if (this.pieChart) {
      this.pieChart.destroy();
    }

    this.pieChart = new Chart(this.pieChartRef!.nativeElement, {
      type: 'pie',
      data: chartData,
      options: {
        responsive: true,
      },
    });

  }
  renderSourceDestinationChart(): void {
    const sourceDestCounts = this.getSourceDestinationCounts(this.logs);
    const sourceDestLabels = Object.keys(sourceDestCounts);
    const sourceDestData = Object.values(sourceDestCounts);
    const chartData: ChartData = {
      labels: sourceDestLabels,
      datasets: [{
        label: 'Source-Destination Counts',
        data: sourceDestData,
        backgroundColor: 'rgba(153, 102, 255, 0.2)',
        borderColor: 'rgba(153, 102, 255, 1)',
        borderWidth: 1
      }]
    };
    const chartOptions: ChartOptions = {
      responsive: true,
      scales: {
        y: {
          beginAtZero: true
        }
      }
    };

    if (this.sourceDestinationChart) {
      this.sourceDestinationChart.destroy();
    }

    this.sourceDestinationChart = new Chart(this.sourceDestinationChartRef.nativeElement, {
      type: 'bar',
      data: chartData,
      options: chartOptions
    });
  }

  getSourceDestinationCounts(logs: any[]): { [key: string]: number } {
    const sourceDestCounts: { [key: string]: number } = {};

    logs.forEach((log: any) => {
      const srcCountry = log.fields?.srccountry?.[0];
      const dstCountry = log.fields?.dstcountry?.[0];
      if (srcCountry && dstCountry) {
        const key = `${srcCountry} -> ${dstCountry}`;
        if (sourceDestCounts[key]) {
          sourceDestCounts[key]++;
        } else {
          sourceDestCounts[key] = 1;
        }
      }
    });

    return sourceDestCounts;
  }

  renderServiceCountChart(): void {
    const serviceCounts = this.getServiceCounts(this.logs);
    const serviceLabels = Object.keys(serviceCounts);
    const serviceData = Object.values(serviceCounts);
    const chartData: ChartData = {
      labels: serviceLabels,
      datasets: [{
        label: 'Service Counts',
        data: serviceData,
        backgroundColor: 'rgba(75, 192, 192, 0.2)',
        borderColor: 'rgba(75, 192, 192, 1)',
        borderWidth: 1
      }]
    };
    const chartOptions: ChartOptions = {
      responsive: true,
      scales: {
        y: {
          beginAtZero: true
        }
      }
    };

    if (this.serviceCountChart) {
      this.serviceCountChart.destroy();
    }

    this.serviceCountChart = new Chart(this.serviceCountChartRef.nativeElement, {
      type: 'line',
      data: chartData,
      options: chartOptions
    });
  }



  getServiceCounts(logs: any[]): { [key: string]: number } {
    const serviceCounts: { [key: string]: number } = {};

    logs.forEach((log: any) => {
      const service = log.fields?.service?.[0];
      if (service) {
        if (serviceCounts[service]) {
          serviceCounts[service]++;
        } else {
          serviceCounts[service] = 1;
        }
      }
    });

    return serviceCounts;
  }


  renderServiceCountByTimeChart(): void {
    const serviceCountsByTime = this.getServiceCountsByTime(this.logs);
    const labels = serviceCountsByTime.map(entry => entry.time);
    const services = Array.from(new Set(this.logs.map(log => log.fields?.service?.[0])));

    const datasets = services.map(service => ({
      label: service,
      data: labels.map(label => {
        const entry = serviceCountsByTime.find(e => e.time === label);
        return entry ? entry.counts[service] || 0 : 0;
      }),
      fill: false,
      borderColor: this.getRandomColor(),
      tension: 0.1
    }));

    const chartData: ChartData = {
      labels: labels,
      datasets: datasets
    };

    const chartOptions: ChartOptions = {
      responsive: true,
      scales: {
        y: {
          beginAtZero: true
        },
        x: {
          type: 'time',
          time: {
            unit: 'second' // Adjust this to 'day', 'month', etc. as needed
          }
        }
      }
    };

    if (this.serviceCountByTimeChart) {
      this.serviceCountByTimeChart.destroy();
    }

    this.serviceCountByTimeChart = new Chart(this.serviceCountByTimeChartRef.nativeElement, {
      type: 'line',
      data: chartData,
      options: chartOptions
    });
  }

  getServiceCountsByTime(logs: any[]): { time: string, counts: { [service: string]: number } }[] {
    const countsByTime: { [key: string]: { [service: string]: number } } = {};

    logs.forEach(log => {
      const timestamp = log.fields?.['@timestamp']?.[0];
      const service = log.fields?.service?.[0];
      if (timestamp && service) {
        const time = moment(timestamp).startOf('second').toISOString(); // Adjust the time unit as needed
        if (!countsByTime[time]) {
          countsByTime[time] = {};
        }
        if (!countsByTime[time][service]) {
          countsByTime[time][service] = 0;
        }
        countsByTime[time][service]++;
      }
    });

    return Object.keys(countsByTime).map(time => ({
      time,
      counts: countsByTime[time]
    })).sort((a, b) => new Date(a.time).getTime() - new Date(b.time).getTime());
  }

  getRandomColor(): string {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  renderThreatLevelChart(): void {
    const threatLevelCounts = this.getThreatLevelCounts(this.logs);
    const labels = Object.keys(threatLevelCounts);
    const data = Object.values(threatLevelCounts);

    const chartData: ChartData = {
      labels: labels,
      datasets: [{
        label: 'Count of Logs',
        data: data,
        fill: false,
        backgroundColor: 'rgba(255, 99, 132, 0.2)',
        borderColor: 'rgb(255, 99, 132)',
        tension: 0.1
      }]
    };

    const chartOptions: ChartOptions = {
      responsive: true,
      scales: {
        y: {
          beginAtZero: true
        }
      }
    };

    if (this.threatLevelChart) {
      this.threatLevelChart.destroy();
    }

    this.threatLevelChart = new Chart(this.threatLevelChartRef.nativeElement, {
      type: 'bar',
      data: chartData,
      options: chartOptions
    });
  }

  getThreatLevelCounts(logs: any[]): { [threatLevel: string]: number } {
    const threatLevelCounts: { [threatLevel: string]: number } = {};

    logs.forEach(log => {
      const threatLevel = log.fields?.loglevel?.[0];
      if (threatLevel) {
        if (!threatLevelCounts[threatLevel]) {
          threatLevelCounts[threatLevel] = 0;
        }
        threatLevelCounts[threatLevel]++;
      }
    });

    return threatLevelCounts;
  }

  renderSourceDestinationIPChart(): void {
    const ipCounts = this.getIPCounts(this.logs);
    const labels = Object.keys(ipCounts);
    const data = Object.values(ipCounts);

    const chartData: ChartData = {
      labels: labels,
      datasets: [{
        label: 'Count of Logs',
        data: data,
        backgroundColor: 'rgba(75, 192, 192, 0.2)',
        borderColor: 'rgba(75, 192, 192, 1)',
        borderWidth: 1
      }]
    };

    const chartOptions: ChartOptions = {
      responsive: true,
      scales: {
        y: {
          beginAtZero: true
        }
      }
    };

    if (this.sourceDestinationIPChart) {
      this.sourceDestinationIPChart.destroy();
    }

    this.sourceDestinationIPChart = new Chart(this.sourceDestinationIPChartRef.nativeElement, {
      type: 'bar',
      data: chartData,
      options: chartOptions
    });
  }

  getIPCounts(logs: any[]): { [ip: string]: number } {
    const ipCounts: { [ip: string]: number } = {};

    logs.forEach(log => {
      const sourceIP = log.fields?.srcip?.[0];
      const destinationIP = log.fields?.dstip?.[0];
      if (sourceIP) {
        if (!ipCounts[sourceIP]) {
          ipCounts[sourceIP] = 0;
        }
        ipCounts[sourceIP]++;
      }
      if (destinationIP) {
        if (!ipCounts[destinationIP]) {
          ipCounts[destinationIP] = 0;
        }
        ipCounts[destinationIP]++;
      }
    });

    return ipCounts;
  }

}
