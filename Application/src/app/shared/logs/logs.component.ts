import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LogService } from 'src/app/services/log/log.service';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.css']
})
export class LogsComponent {
  serviceName!: string;
  logs: any[] = [];
  currentPage: number = 1;
  pageSize: number = 10;
  totalPages: number = 0;
  maxPageNumbersDisplayed: number = 5; // Adjust this value to change the number of page numbers displayed

  constructor(private route: ActivatedRoute, private ls: LogService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.serviceName = params['service-name'];
      this.viewLogs(this.serviceName);
    });
  }

  viewLogs(serviceName: string): void {
    this.ls.getLogs(serviceName).subscribe(
      data => {
        this.logs = data.response.hits.hits.map((log: any) => ({
          ...log,
          showDetails: false
        }));
        this.totalPages = Math.ceil(this.logs.length / this.pageSize);
      },
      error => {
        console.error('Error fetching logs', error);
      }
    );
  }

  getPaginatedLogs(): any[] {
    const startIndex = (this.currentPage - 1) * this.pageSize;
    return this.logs.slice(startIndex, startIndex + this.pageSize);
  }

  getObjectKeys(obj: any): string[] {
    return Object.keys(obj);
  }

  onPageChange(page: number): void {
    if (page >= 1 && page <= this.totalPages) {
      this.currentPage = page;
    }
  }

  getPageNumbers(): number[] {
    const pages: number[] = [];
    let startPage: number, endPage: number;

    if (this.totalPages <= this.maxPageNumbersDisplayed) {
      // Display all pages if the total number of pages is less than or equal to maxPageNumbersDisplayed
      startPage = 1;
      endPage = this.totalPages;
    } else {
      // Determine the start and end page numbers to display around the current page
      const halfDisplayedPages = Math.floor(this.maxPageNumbersDisplayed / 2);
      if (this.currentPage <= halfDisplayedPages) {
        startPage = 1;
        endPage = this.maxPageNumbersDisplayed;
      } else if (this.currentPage + halfDisplayedPages >= this.totalPages) {
        startPage = this.totalPages - this.maxPageNumbersDisplayed + 1;
        endPage = this.totalPages;
      } else {
        startPage = this.currentPage - halfDisplayedPages;
        endPage = this.currentPage + halfDisplayedPages;
      }
    }

    if (startPage > 1) {
      pages.push(1);
      if (startPage > 2) {
        pages.push(-1); // Use -1 as a placeholder for ellipses
      }
    }

    for (let i = startPage; i <= endPage; i++) {
      pages.push(i);
    }

    if (endPage < this.totalPages) {
      if (endPage < this.totalPages - 1) {
        pages.push(-1); // Use -1 as a placeholder for ellipses
      }
      pages.push(this.totalPages);
    }

    return pages;
  }


  toggleDetails(log: any): void {
    log.showDetails = !log.showDetails;
  }

  getSummary(fields: any, service: string): string {
    let summaryKeys: string[] = [];

    // Determine which summary keys to use based on the service
    if (service === 'fortigate') {
      summaryKeys = ['@timestamp', 'action', 'app', 'srcip', 'dstip'];
    } else if (service === 'mysql') {
      summaryKeys = ['@timestamp', 'host.hostname', 'host.ip', 'agent.type', 'service.type.keyword'];
    }

    return summaryKeys.map(key => `${key}: ${fields[key] !== undefined ? fields[key][0] : ''}`).join(', ');
  }
}
