import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogsComponent } from './logs/logs.component';
import { ChartsComponent } from './charts/charts.component';




@NgModule({
  declarations: [
    ChartsComponent,
    LogsComponent

  ],
  imports: [
    CommonModule,
  ],
  exports: [
    ChartsComponent,
    LogsComponent
  ]
})
export class SharedModule { }
