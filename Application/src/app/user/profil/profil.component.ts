import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user/user.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent {
  profileForm!: FormGroup;
  userId!: number;
  user: any = {};

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.profileForm = this.formBuilder.group({
      prenom: ['', Validators.required],
      nom: ['', Validators.required],
      contact: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      departement: ['', Validators.required],
      password: ['', Validators.required]
    });

    this.route.paramMap.subscribe(params => {
      const userIdParam = params.get('id_user');
      this.userId = userIdParam ? +userIdParam : 0;
      if (this.userId) {
        this.loadUserData(this.userId);
      }
    });
  }

  loadUserData(userId: number): void {
    this.userService.getUserById(userId).subscribe(user => {
      if (user) {
        this.user = user;
        this.profileForm.patchValue({
          prenom: user.prenom,
          nom: user.nom,
          contact: user.contact,
          email: user.email,
          departement: user.departement,
         // password: user.password // Assuming you want to populate the password field
        });
      } else {
        console.error('User data is empty or undefined');
      }
    }, error => {
      console.error('Error fetching user data:', error);
    });
  }

  onSubmit(): void {
    if (this.profileForm.valid) {
      const userData = this.profileForm.value;
      userData.id_user = this.userId; // Add user ID to the data object

      this.userService.editUser(userData).subscribe(
        response => {
          console.log('Profile updated!', response);
        },
        error => {
          console.error('Error updating profile:', error);
        }
      );
    }
  }
}
