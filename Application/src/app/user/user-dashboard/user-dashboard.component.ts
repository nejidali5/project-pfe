import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { LoginService } from 'src/app/services/login/login.service';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css']
})
export class UserDashboardComponent {

  user: any;

  constructor(private log: LoginService, private router: Router) { }

  logout() {
    this.log.deconnexion()

  }
  checkUser() {
    this.log.controle().subscribe(
      (response) => {
        if (response) {
          this.user = this.log.user;
        }
      }
    )

  }


  ngOnInit(): void {
    this.checkUser()
    const sidebar = document.querySelector(".sidebar") as HTMLElement;
    const closeBtn = document.querySelector("#btn") as HTMLElement;
    const searchBtn = document.querySelector(".bx-search") as HTMLElement;
    closeBtn.addEventListener("click", () => {
      sidebar.classList.toggle("open");
      this.menuBtnChange();
    });

    searchBtn.addEventListener("click", () => {
      sidebar.classList.toggle("open");
      this.menuBtnChange();
    });
  }

  menuBtnChange() {
    const sidebar = document.querySelector(".sidebar") as HTMLElement;
    const closeBtn = document.querySelector("#btn") as HTMLElement;

    if (sidebar.classList.contains("open")) {
      closeBtn.classList.replace("bx-menu", "bx-menu-alt-right");
    } else {
      closeBtn.classList.replace("bx-menu-alt-right", "bx-menu");
    }
  }

}



