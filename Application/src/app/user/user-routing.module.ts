import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserDashboardComponent } from './user-dashboard/user-dashboard.component';
import { LogsComponent } from '../shared/logs/logs.component';
import { ChartsComponent } from '../shared/charts/charts.component';
import { UserServicesComponent } from './user-services/user-services.component';
import { ProfilComponent } from './profil/profil.component';

const routes: Routes = [
  {
    path: '',
    component: UserDashboardComponent,
    children: [
      { path: 'logs/:service-name', component: LogsComponent },
      { path: 'charts/:service-name', component: ChartsComponent },
      { path: 'services', component: UserServicesComponent },
      { path: 'profil/:id_user', component: ProfilComponent },

      //{ path: 'liste-services', component: ListServicesComponent }, change with user services.
      //{ path: 'liste-users', component: ListUsersComponent }, change with user profile.



    ]
  }




];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
