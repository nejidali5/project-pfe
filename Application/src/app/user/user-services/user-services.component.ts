import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from 'src/app/services/service/service.service';

@Component({
  selector: 'app-user-services',
  templateUrl: './user-services.component.html',
  styleUrls: ['./user-services.component.css']
})
export class UserServicesComponent {
  listeServices!: any[];

  constructor(private ss: ServiceService, private rt: Router) { }

  ngOnInit(): void {
    this.loadServices();
  }

  loadServices() {
    this.ss.getServices().subscribe(data => {
      this.listeServices = data;
    });
  }


  viewLogs(serviceName: string): void {
    this.rt.navigate(['/user/logs/', serviceName]);
  }

  viewCharts(serviceName: string): void {
    this.rt.navigate(['/user/charts/', serviceName]);
  }
}
