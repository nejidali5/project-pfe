const express = require('express');
const bodyParser = require('body-parser');
const app = express();

// Node JS API initialising
app.use(express.json())
app.use(bodyParser.json());
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*'); // Replace * with the appropriate origin
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Credentials', true);
  if (req.method === 'OPTIONS') {
    return res.sendStatus(200); // Respond to preflight requests
  }
  next();
});

const port = 3000
////-----------------------------------------------



// Routes
const serviceRoutes = require('./src/routes/serviceRoutes');
const userRoutes = require('./src/routes/userRoutes');
const logRoutes = require('./src/routes/logRoutes');
const authRoutes = require('./src/routes/authRoutes');

app.use('/api/users', userRoutes);
app.use('/api/services', serviceRoutes);
app.use('/api/logs', logRoutes);
app.use('/api/auth', authRoutes);





app.listen(port, () => {
  console.log(`Server Start with URL : http://localhost:${port}/`);
});
