const loginService = require('../services/loginService');
const signupService = require('../services/signupService');

exports.login = async (req, res) => {
  await loginService.login(req, res);
};

exports.signup = async (req, res) => {
  await signupService.signup(req, res);
};
