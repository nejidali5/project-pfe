const logModel = require('../models/log');

exports.fetchLogs = async (req, res) => {
  await logModel.fetchLogs(req, res);
}
