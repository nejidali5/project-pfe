const serviceModel = require('../models/service');

exports.getServices = async (req, res) => {
  await serviceModel.getServices(req, res);
};

exports.getService = async (req, res) => {
  await serviceModel.getService(req, res);
};

exports.addService = async (req, res) => {
  await serviceModel.addService(req, res);
};

exports.updateService = async (req, res) => {
  await serviceModel.updateService(req, res);
};

exports.deleteService = async (req, res) => {
  await serviceModel.deleteService(req, res);
};
