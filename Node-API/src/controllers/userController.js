const userModel = require('../models/user');

exports.getUsers = async (req, res) => {
  await userModel.getUsers(req, res);
};

exports.getUser = async (req, res) => {
  await userModel.getUser(req, res);
};

exports.addUser = async (req, res) => {
  await userModel.addUser(req, res);
};

exports.updateUser = async (req, res) => {
  await userModel.updateUser(req, res);
};

exports.deleteUser = async (req, res) => {
  await userModel.deleteUser(req, res);
};
