const path = require('path');
const dotenv = require('dotenv');
const util = require('util');
dotenv.config();
const http = require('http');


const ELASTICSEARCH_HOST = process.env.ELASTICSEARCH_HOST;
const ELASTICSEARCH_PORT = process.env.ELASTICSEARCH_PORT;

exports.fetchLogs = (req, res) => {
  const indexName = req.params.name;
  const postData = JSON.stringify({
    "sort": [{ "_score": { "order": "desc" } }],
    "track_total_hits": false,
    "fields": [
      { "field": "*", "include_unmapped": "true" },
      { "field": "@timestamp", "format": "strict_date_optional_time" }
    ],
    "size": 500,
    "version": true,
    "script_fields": {},
    "stored_fields": ["*"],
    "runtime_mappings": {},
    "_source": false,
    "query": {
      "bool": {
        "must": [],
        "filter": [],
        "should": [],
        "must_not": []
      }
    },
    "highlight": {
      "pre_tags": ["@kibana-highlighted-field@"],
      "post_tags": ["@/kibana-highlighted-field@"],
      "fields": { "*": {} },
      "fragment_size": 2147483647
    }
  });

  const options = {
    hostname: ELASTICSEARCH_HOST,
    port: ELASTICSEARCH_PORT,
    path: `/${indexName}/_async_search?batched_reduce_size=64&ccs_minimize_roundtrips=true&wait_for_completion_timeout=200ms&keep_on_completion=true&keep_alive=60000ms&ignore_unavailable=true&preference=1716197725325`,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': Buffer.byteLength(postData)
    }
  };

  const reqHttp = http.request(options, (resHttp) => {
    let data = '';

    resHttp.on('data', (chunk) => {
      data += chunk;
    });

    resHttp.on('end', () => {
      res.send(JSON.parse(data));
      console.log("Logs fetched successfully");
    });
  });

  reqHttp.on('error', (e) => {
    console.log("Error fetching logs:", e);
    res.status(500).send({ error: "Error fetching logs" });
  });

  // Write data to request body
  reqHttp.write(postData);
  reqHttp.end();
};
