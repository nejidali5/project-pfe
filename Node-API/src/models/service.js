const connection = require('../config/database');
const path = require('path');
const fs = require('fs');
const dotenv = require('dotenv');
const util = require('util');
const { exec } = require('child_process');

dotenv.config();

reload_logstash_config = async () => {
  const command = `docker exec logstash sh -c "kill -SIGHUP $(pidof logstash)"`;

  exec(command, (error, stdout, stderr) => {
    if (error) {
      console.error(`exec error: ${error}`);
      res.status(500).send(`Error: ${error.message}`);
      return;
    }
    if (stderr) {
      console.error(`stderr: ${stderr}`);
      res.status(500).send(`Error: ${stderr}`);
      return;
    }
    console.log(`stdout: ${stdout}`);
    res.send('Logstash configuration reloaded successfully');
  });
}

exports.getServices = async (req, res) => {
  connection.query('SELECT * FROM service', (err, rows) => {
    if (err) {
      console.log("E: Error getting services");
    };
    res.send(rows);
    console.log("All services aquired successfully");
  });
}

exports.getService = async (req, res) => {
  const serviceId = req.params.id;
  const query = 'SELECT * FROM service WHERE id_service = ?';
  connection.query(query, [serviceId], (error, results) => {
    if (error) {
      console.log("E: Error getting service [id=", serviceId, "]")
      res.status(500).json({ error });
      return;
    }
    console.log("Service aquired successfully");
    res.json(results[0]);
  });
};



exports.updateService = async (req, res) => {

  let data = req.body;
  let oldName = data.oldName;
  delete data.oldName;
  let idService = req.body.id_service;
  editPipeline(data, oldName);

  connection.query('UPDATE service SET ? WHERE id_service = ?', [data, idService], (err, rows) => {
    if (err) {
      console.log('E: Error updating service [id=', idService, "]");
      res.status(500).send('Error updating service');
      return;
    }
    console.log("Service updated successfully");
    res.send(rows);
  });
  updatePipelinesYaml();
  reload_logstash_config();
}


exports.deleteService = async (req, res) => {
  const id = req.params.id;
  const name = req.params.name;
  deletePipeline(name)
  connection.query('DELETE FROM service WHERE id_service = ?', id, (err, results) => {
    if (err) {
      console.log('E: Error deleting service [id=', id, "]");
      res.status(500).json({ error: 'An error occurred while deleting the service.' });
    } else {
      console.log("Service deleted successfully [" + id + ']');
      res.status(200).json({ message: 'Service deleted successfully' });
    }
  });
  updatePipelinesYaml();
  reload_logstash_config();
}

exports.addService = async (req, res) => {
  let data = req.body;
  createPipeline(data.input, data.filter, data.output, data.name);
  connection.query('INSERT INTO service SET ?', data, (err, rows) => {
    try {
      res.send(rows);
    }
    catch (error) {
      console.log("E: Error adding the service");
    }
  });
  updatePipelinesYaml();
  console.log("Service added successfully");
  reload_logstash_config();
}

updatePipelinesYaml = async () => {
  const pipelineYamlFile = process.env.pipelinesYamlFile;
  const configFolder = process.env.LogFolder;
  const rows = await util.promisify(connection.query).bind(connection)('SELECT name FROM service');
  let content = '';

  for (const row of rows) {
    content += `- pipeline.id: ${row.name}\n  path.config: "${configFolder}${row.name}.config"\n`;
  }

  //fs.unlink(pipelineYamlFile);
  x = await fs.unlink(pipelineYamlFile, (err) => {
    if (err) {
      console.log("E: Error deleting pipelines.yml file");
    }
  });
  fs.writeFile(pipelineYamlFile, content, err => {
    if (err) {
      console.log("E: Error updating the pipelines.yml file");
    }
  });
  console.log("Pipelines.yml updated successfully");
  reload_logstash_config();
}

createPipeline = async (input, filter, output, name) => {
  const configFolder = process.env.LogFolder;
  const content = "input{" + input + "\n}\nfilter{" + filter + '\n}\noutput{\n' + output + '\n}'
  fs.writeFile(configFolder + name + '.conf', content, err => {
    if (err) {
      console.log("E: Error creating the pipeline");
    }
  });
  console.log("Pipeline created successfully [" + name + '.conf]');
  reload_logstash_config();
}

deletePipeline = async (name) => {
  const configFolder = process.env.LogFolder;
  fs.unlink(configFolder + name + '.conf', (err) => {
    if (err) {
      console.log("E: Error deleting pipeline [" + name + '.conf]');
    }
  });
  console.log("Pipeline deleted successfully [" + name + ".conf]");
  reload_logstash_config();
}

editPipeline = async (data, oldName) => {
  console.log("=============== Pipeline edited ===============");
  await deletePipeline(oldName);
  createPipeline(data.input, data.filter, data.output, data.name);
  console.log("=============== End of Pipeline edit ===========");
  reload_logstash_config();
}
