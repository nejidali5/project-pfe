const bcrypt = require('bcrypt');
const connection = require('../config/database');

exports.getUsers = async (req, res) => {
  connection.query('SELECT * FROM user', (err, rows) => {
    if (err) throw err;
    res.send(rows);
  });
}

exports.getUser = async (req, res) => {
  const userId = req.params.id;

  connection.query('SELECT * FROM user WHERE id_user = ?', [userId], (err, rows) => {
    if (err) {
      console.error('Error fetching user:', err);
      return res.status(500).json({ message: 'Server error' });
    }
    if (rows.length === 0) {
      return res.status(404).json({ message: 'User not found' });
    }
    res.json(rows[0]); // Assuming rows contains the user data
  });
};

exports.addUser = async (req, res) => {
  let data = req.body;
  data.password = await bcrypt.hash(data.password, 10);
  connection.query('INSERT INTO user SET ?', data, (err, rows) => {
    if (err) throw err;
    res.send(rows);
  });
}


exports.updateUser = async (req, res) => {
  let data = { ...req.body }; // Create a copy of req.body to avoid modifying it directly

  // Remove id from the data object if it exists
  delete data.id_user;

  if (data.password) {
    data.password = await bcrypt.hash(data.password, 10);
  }

  connection.query('UPDATE user SET ? WHERE id_user = ?', [data, req.body.id_user], (err, rows) => {
    if (err) {
      console.error('Error updating user:', err);
      return res.status(500).json({ message: 'Server error' });
    }
    res.send(rows);
  });
}

exports.deleteUser = async (req, res) => {
  const { id } = req.params;
  connection.query('DELETE FROM user WHERE id_user = ?', id, (err, results) => {
    if (err) {
      res.status(500).json({ error: 'An error occurred while deleting the user.' });
    } else {
      res.status(200).json({ message: 'User deleted successfully' });
    }
  });

}

