const express = require('express');
const authController = require('../controllers/authController');
const tokenController = require('../controllers/tokenController');
const router = express.Router();

router.post('/login', authController.login);
router.post('/signup', authController.signup);
router.post('/verifyToken', tokenController.verifyToken);

module.exports = router;
