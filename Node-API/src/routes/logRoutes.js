const express = require('express');
const logController = require('../controllers/logController');
const router = express.Router();

router.get('/:name', logController.fetchLogs);

module.exports = router;
