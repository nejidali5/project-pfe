const express = require('express');
const serviceController = require('../controllers/serviceController');
const router = express.Router();

router.get('/', serviceController.getServices);
router.get('/:id', serviceController.getService);
router.post('/addService', serviceController.addService);
router.post('/editService', serviceController.updateService);
router.delete('/:id/:name', serviceController.deleteService);

module.exports = router;
